import React, { PureComponent } from 'react';
import Background from './images/deco.jpg';

const styles = {
  container: {
    width: 800,
    height: 600,
    margin: '0 auto',
    backgroundImage: "url(" + Background + ")"
  }
};

class App  extends PureComponent {
  render() {
    return (
        <div style={styles.container}>
          <form>
            <label>
              Nom :
              <input type="text" name="name" />
            </label>
            <input type="submit" value="Envoyer" />
          </form>
        </div>
    );
  }
}

export default App;
